export const ADD_TODO = 'ADD_TODO'
export const FETCH_TODO = 'FETCH_TODO'

export const addTodo = (taskName) => {
    return {
        type: ADD_TODO,
        payLoad: {taskName}
    }
}

export const fetchTodos = () => {
    return {
        type: FETCH_TODO
    }
}