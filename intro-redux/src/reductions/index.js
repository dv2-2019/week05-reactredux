import { combineReducers } from 'redux';
import { CounterReducer } from './CounterReducer.js'
import { TodoReducer } from './TodoReducer.js';

export const rootReducer = combineReducers({
    counter: CounterReducer,
    todos: TodoReducer
})