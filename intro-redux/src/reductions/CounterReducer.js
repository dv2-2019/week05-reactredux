import { INCREMENT, DECREMENT } from "../actions/CounterAction"

const InitialCount = 0
export const CounterReducer = (state = InitialCount, action) => {
    switch (action.type) {
        case INCREMENT:
            return state + action.payLoad

        case DECREMENT:
            return state -  action.payLoad
        default:
            return state
    }
}