import { ADD_TODO, FETCH_TODO } from "../actions/TodoAction"

const initialTodos = []
export const TodoReducer = (state = initialTodos, action) => {
    switch (action.type) {
        case ADD_TODO:
            return [action.payLoad, ...state]

        case FETCH_TODO:
            const todoFetchFromServer = [
                { taskName: 'task 1' },
                { taskName: 'task 2' },
                { taskName: 'task 3' }]

                return todoFetchFromServer
        default:
            return state
    }
}