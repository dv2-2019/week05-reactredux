import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { increment, decrement } from '../actions/CounterAction';

const CounterRedux = () => {
    const counter = useSelector(state => state.counter)

    const dispatch = useDispatch();

    return (
        <div className="App">
            <h3>Counter Redux </h3>
            <div>Countes = {counter}</div>
            <div>
                <button onClick={() => dispatch(decrement(5))}>-</button>
                <button onClick={() => dispatch(increment(5))}>+</button>
            </div>
        </div>
    );
}

export default CounterRedux;