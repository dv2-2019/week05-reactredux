import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { addTodo } from '../actions/TodoAction';

const TodoForm = () => {
    const [newTodoName, setNewTodoName] = useState('')
    // const todos = useSelector(state => state.todos)
    const dispatch = useDispatch();

    const clickedAddHandler = () => {
        dispatch(addTodo(newTodoName))
        setNewTodoName('')
    }

    return (
        <div>
            <input value={newTodoName} onChange={(e) => setNewTodoName(e.target.value)} ></input>
            <button onClick={clickedAddHandler}>ADD</button>
        </div>
    )
}
export default TodoForm;