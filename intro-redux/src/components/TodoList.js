import React from 'react';
import { useSelector} from 'react-redux';

const TodoList = () => {

    const todos = useSelector(state => state.todos)

    return (
        <div>
            {
                todos.map((todo) => {
                    return <h4>{todo.taskName}</h4>
                })
            }
        </div>
    );
}

export default TodoList;