import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import TodoForm from './TodoForm';
import TodoList from './TodoList';
import { fetchTodos } from '../actions/TodoAction';
const TodoRedux = () => {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchTodos())
    }, [])
    return (
        <div>
            <h3>Todo </h3>
            <TodoForm />
            <TodoList></TodoList>
        </div>
    );
}

export default TodoRedux;