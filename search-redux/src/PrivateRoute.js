
import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { isLogin, isLogout } from './actions/AuthAction';
import { connect } from 'react-redux';

const PrivateRoute = ({ path, component: Component, exact, localStorage }) => (
    <Route path={path} exact={exact} render={(props) => {
        // const isLogin = localStorage

        if (localStorage == 'true') {
            return <Component {...props} />
        } else {
            return <Redirect to="/login" />
        }
    }} />
)

const mapStateToProps = state => {
    return {
        localStorage: state.login.localStorage
    }
}

export default connect(mapStateToProps)(PrivateRoute);