export const FETCH_USERS_BEGIN = 'FETCH_USERS_BEGIN'
export const FETCH_USERS_SUCCESS = 'FETCH_USERS_SUCCESS'
export const FETCH_USERS_ERROR = 'FETCH_USERS_ERROR'

export const fetchUsersList = () => {
    return dispatch => {
        dispatch(fetchUsersListBegin())
        return fetch('http://jsonplaceholder.typicode.com/users')
            .then(res => res.json())
            .then(data => {
                dispatch(fetchUsersListSuccess(data))
            })
            .catch(error => dispatch(fetchUsersListError(error)))
    }
}

export const fetchUsersListBegin = () => {
    return {
        type: FETCH_USERS_BEGIN,
    }
}

export const fetchUsersListSuccess = (data) => {
    return {
        type: FETCH_USERS_SUCCESS,
        payLoad: data
    }
}

export const fetchUsersListError = error => {
    return {
        type: FETCH_USERS_ERROR,
        payLoad: error
    }
}