export const FETCH_TODOS_BEGIN = 'FETCH_TODOS_BEGIN'
export const FETCH_TODOS_SUCCESS = 'FETCH_TODOS_SUCCESS'
export const FETCH_TODOS_ERROR = 'FETCH_TODOS_ERROR'


export const SEARCH_DATA = 'SEARCH_DATA'
export const FILTER_LIST = 'FILTER_LIST'
export const CLICK_DONE = 'CLICK_DONE'

export const fetchTodos = (userId) => {
    return dispatch => {
        dispatch(fetchTodoBegin())
        return fetch('http://jsonplaceholder.typicode.com/todos?userId=' + userId)
            .then(res => res.json())
            .then(data => {
                dispatch(fetchTodoSuccess(data))
            })
            .catch(error => dispatch(fetchTodoError(error)))
    }
}

export const fetchTodoBegin = () => {
    return {
        type: FETCH_TODOS_BEGIN,
    }
}

export const fetchTodoSuccess = data => {
    return {
        type: FETCH_TODOS_SUCCESS,
        payLoad: data
    }
}
export const fetchTodoError = error => {
    return {
        type: FETCH_TODOS_ERROR,
        payLoad: error
    }
}



//actions
export const filterList = (value) => {
    return {
        type: FILTER_LIST,
        payLoad: value
    }
}

export const searchData = (value) => {
    return {
        type: SEARCH_DATA,
        payLoad: value
    }
}

export const clickDone = (index) => {
    return {
        type: CLICK_DONE,
        payLoad: index
    }
}