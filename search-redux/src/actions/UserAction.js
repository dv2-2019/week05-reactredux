
export const FETCH_USER_BEGIN = 'FETCH_USER_BEGIN'
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS'
export const FETCH_USER_ERROR = 'FETCH_USER_ERROR'

export const fetchUser = (userId) => {
    return dispatch => {
        dispatch(fetchUserBegin())
        return fetch('http://jsonplaceholder.typicode.com/users/' + userId)
            .then(res => res.json())
            .then(data => {
                dispatch(fetchUserSuccess(data))
            })
            .catch(error => dispatch(fetchUserError(error)))
    }
}

export const fetchUserBegin = () => {
    return {
        type: FETCH_USER_BEGIN,
    }
}

export const fetchUserSuccess = (data) => {
    return {
        type: FETCH_USER_SUCCESS,
        payLoad: data
    }
}

export const fetchUserError = error => {
    return {
        type: FETCH_USER_ERROR,
        payLoad: error
    }
}


