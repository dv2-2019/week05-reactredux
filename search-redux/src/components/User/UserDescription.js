import React from 'react'
import 'antd/dist/antd.css';
import { useSelector } from 'react-redux';
import { Descriptions, Col } from 'antd';

const UserDescription = () => {
    const userInfo = useSelector(state => state.userInfo.user)

    return (

        <Col span={18} push={6}>
            <Descriptions layout="vertical">
                <Descriptions.Item label="Name">{userInfo.name}</Descriptions.Item>
                <Descriptions.Item label="User Name">{userInfo.username}</Descriptions.Item>
                <Descriptions.Item label="Email">{userInfo.email}</Descriptions.Item>
                <Descriptions.Item label="Website">{userInfo.website}</Descriptions.Item>
                <Descriptions.Item label="Telephone">{userInfo.phone}</Descriptions.Item>
                {
                    userInfo.address != undefined ?
                        <Descriptions.Item label="Address">{userInfo.address.street}
                            {userInfo.address.suite} ,
                                    {userInfo.address.city} ,
                                    {userInfo.address.zipcode}
                        </Descriptions.Item>
                        :
                        ""
                }
            </Descriptions>
        </Col>
    )

}
export default UserDescription;