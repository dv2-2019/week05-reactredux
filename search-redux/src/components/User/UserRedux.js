
import React, { useEffect } from 'react';
import 'antd/dist/antd.css';
import { useSelector, useDispatch } from 'react-redux';
import { fetchUsersList } from '../../actions/UsersListAction';
import SearchForm from '../SearchForm';

//ant design
import { Layout, Menu, Row, Card, Icon, List, Avatar, Skeleton, Button } from 'antd';

const { Header } = Layout;
const UserRedux = () => {

    const usersList = useSelector(state => state.usersList.users)
    const search = useSelector(state => state.search)
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchUsersList());
    }, [])

    return (
        <Row style={{ backgroundColor: '#e6e6e6' }}>
            <Header className="header">
                <Menu
                    theme="dark"
                    mode="horizontal"
                    defaultSelectedKeys={['2']}
                    style={{ lineHeight: '64px' }}
                    title=" User">
                    <Icon type="user" style={{ fontSize: '22px', color: '#ffff' }} />
                    <Button type="danger" value="Login" className="login-form-button"  style={{ float: 'right', margin: 15 }}
                        onClick={
                            () => {
                                window.location = "/processLogout"
                            }
                        } >
                        Log out</Button>
                </Menu>
            </Header>
            <Card title="User Page" hoverable style={{ padding: '1em', margin: '4em' }} >
                <Row>
                    <SearchForm />
                </Row>

                <List
                    className="demo-loadmore-list"
                    itemLayout="horizontal"
                    dataSource={
                        search ?
                            usersList.filter(m => m.name.match(search))
                            :
                            usersList
                    }
                    renderItem={item => (
                        <List.Item
                            actions={[<a href={"/users/" + item.id + "/todo"}>Todo</a>, <a href={"/users/" + item.id + "/albums"}>Albums</a>]}>
                            <Skeleton avatar title={false} loading={item.loading} active>
                                <List.Item.Meta
                                    avatar={
                                        <Avatar src="https://img.icons8.com/bubbles/2x/system-administrator-female.png" />}
                                    title={<a href="https://ant.design">{item.name}</a>}
                                    description={item.email} />
                                <div>{item.phone} | {item.website}</div>
                            </Skeleton>

                        </List.Item>
                    )}
                />
            </Card>
        </Row>
    )

}
export default UserRedux;