import React, { useEffect } from 'react';
import 'antd/dist/antd.css';
import { fetchUser } from '../../actions/UserAction';
import { fetchTodos, fetchTodoSuccess } from '../../actions/TodoAction'
import { useSelector, useDispatch } from 'react-redux';

import { List, Typography, Button } from 'antd';

const TodoList = (props) => {
    const dispatch = useDispatch();
    const todoList = useSelector(state => state.todoList.todos)
    const filter = useSelector(state => state.filter)
    const search = useSelector(state => state.search)

    const userId = props.userId;
    console.log(props)

    useEffect(() => {
        dispatch(fetchUser(userId));
        dispatch(fetchTodos(userId));
    }, [])

    const isDoneTodo = (value) => {
        // alert('Done', value)
        let newList = [...todoList];
        newList[value].completed = true;
        dispatch(fetchTodoSuccess(newList))
    }


    let renderItems = todoList

    renderItems = filter == -1 ?
        renderItems
        :
        renderItems.filter(m => m.completed == filter);

    renderItems = search ?
        renderItems.filter(m => m.title.match(search))
        :
        renderItems
    return (

        <List
            header={<div>Todo List</div>}
            footer={<div>Footer</div>}
            bordered
            dataSource={
                renderItems
            }
            renderItem={(item, index) => (
                <List.Item>
                    <div>
                        {
                            item.completed ?
                                <Typography.Text delete >DONE {item.title}</Typography.Text>
                                :
                                <Typography.Text mark > DOING {item.title}</Typography.Text>
                        }
                    </div>
                    <div>
                        {
                            item.completed ?
                                <div>COMPLETED</div>
                                :
                                <Button type="primary" ghost style={{ float: 'right' }}
                                    value={index} onClick={() => isDoneTodo(item.id - 1)} >
                                    <span>Done</span>
                                </Button>
                            // <ButtonClicked></ButtonClicked>
                        }
                    </div>
                </List.Item>
            )}
        />
    )
}
export default TodoList;