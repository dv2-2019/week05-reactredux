
import React  from 'react';
import 'antd/dist/antd.css';
import SearchForm from '../SearchForm';
import FilterTodoList from '../FilterTodoList';
import UserDescription from '../User/UserDescription';
import PageHeaderBar from '../PageHeaderBar';
import TodoList from '../Todo/TodoList';

//ant design
import {  Card, Col, Avatar, Row } from 'antd';



const TodoRedux = (props) => {

    const userId = props.match.params.user_id;

    return (
        <Row style={{ backgroundColor: '#e6e6e6' }}>
            <PageHeaderBar />
            <Card title="User Info" hoverable style={{ padding: '1em', marginTop: '4em', marginRight: '4em', marginLeft: '4em' }}>
                <Row>
                    <UserDescription />
                    <Col span={6} pull={18}>
                        <Avatar style={{ width: 200, height: 200 }} src="https://img.icons8.com/bubbles/2x/system-administrator-female.png" />
                    </Col>
                </Row>
            </Card>

            <Card hoverable style={{ padding: '1em', marginTop: '1em', marginRight: '4em', marginLeft: '4em' }}>
                <SearchForm />
                <FilterTodoList />
                <TodoList userId={userId} />
            </Card>
        </Row>
    )
}
export default TodoRedux;