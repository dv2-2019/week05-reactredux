import React from 'react';
import { PageHeader, Button } from 'antd';

const PageHeaderBar = () => {
    return (
        <PageHeader
            ghost={false}
            onBack={() => window.history.back()}
            title="Back">
            <Button type="danger" value="Login" className="login-form-button" style={{ float: 'right' }}
                onClick={
                    () => {
                        window.location = "/processLogout"
                    }
                } >
                Log out</Button>
        </PageHeader>
    )
}
export default PageHeaderBar;