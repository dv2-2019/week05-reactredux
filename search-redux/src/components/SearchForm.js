import React, { useState } from 'react';
import 'antd/dist/antd.css';
import { useDispatch } from 'react-redux';
import { searchData } from '../actions/TodoAction';
import { Input } from 'antd';

const { Search } = Input;
const SearchForm = () => {
    const dispatch = useDispatch();

    const onSearch = event => {
        console.log(event.target.value)
        dispatch(searchData(event.target.value))
    }

    return (
        <Search
            placeholder="input search text"
            onSearch={value => console.log(value)}
            style={{ width: 300, margin: '1em' }} onChange={onSearch} />
    )
}
export default SearchForm;