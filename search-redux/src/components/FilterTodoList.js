import React, { useState } from 'react';
import 'antd/dist/antd.css';
import { useDispatch } from 'react-redux';
import { filterList } from '../actions/TodoAction';
import { Select } from 'antd';

const { Option } = Select;

const FilterTodoList = () => {
    const dispatch = useDispatch();

    const onSelectChange = (value) => {
        console.log(value)
        dispatch(filterList(value));
    }

    return (
        <Select defaultValue="All" style={{ width: 120 }} onChange={onSelectChange}>
            <Option value={-1}>All</Option>
            <Option value={true}>Done</Option>
            <Option value={false}>Doing</Option>
        </Select>
    )
}
export default FilterTodoList;