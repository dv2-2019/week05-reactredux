import { FETCH_USERS_SUCCESS, FETCH_USERS_BEGIN, FETCH_USERS_ERROR } from "../../actions/UsersListAction"


const initialState = {
    users: [],
    loading: false,
    error: ''
}

export const usersListReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_USERS_SUCCESS:
            return {
                users: action.payLoad,
                loading: false,
                error: ''
            }
        case FETCH_USERS_BEGIN:
            return {
                ...state,
                loading: true
            }
        case FETCH_USERS_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payLoad
            }
        default:
            return state
    }
}
