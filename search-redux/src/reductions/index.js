import { combineReducers } from 'redux';
import { userReducer } from './Todo/UserReducer';
import { todosReducer } from './Todo/TodoReducer';
import { searchReducer } from './Function/SearchReducer';
import { filterReducer } from './Function/FilterReducer';
import { usersListReducer } from '../reductions/User/UsersListReducer';
import { authReducer } from './Function/AuthReducer';

export const rootReducer = combineReducers({
    userInfo: userReducer,
    todoList: todosReducer,
    search: searchReducer,
    filter: filterReducer,
    usersList: usersListReducer,
    login: authReducer

})