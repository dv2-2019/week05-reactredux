import { LOGIN, LOGOUT } from '../../actions/AuthAction';

const initialState = {
    localStorage: localStorage.getItem("isLogin")
}

export const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN:
            localStorage.setItem("isLogin", true)
            return {
                ...state,
                localStorage: localStorage.getItem("isLogin")
            }
        case LOGOUT:
            localStorage.setItem("isLogin", false)
            return {
                ...state,
                localStorage: localStorage.getItem("isLogin")
            }
        default: return state
    }
}