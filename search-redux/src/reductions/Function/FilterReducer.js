import { FILTER_LIST } from "../../actions/TodoAction"

const initialValue = -1;
export const filterReducer = (state = initialValue, action) => {
    switch (action.type) {
        case FILTER_LIST:
            return action.payLoad
        default:
            return state
    }
}