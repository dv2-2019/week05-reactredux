import { FETCH_TODOS_SUCCESS,FETCH_TODOS_BEGIN, FETCH_TODOS_ERROR  } from "../../actions/TodoAction"


const initialState = {
    todos: [],
    loading: false,
    error: ''
}

export const todosReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TODOS_BEGIN:
            return {
                ...state,
                loading: true
            }
        case FETCH_TODOS_SUCCESS:
            return {
                todos: action.payLoad,
                loading: false,
                error: ''
            }
        case FETCH_TODOS_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payLoad
            }
        default:
            return state
    }
}

