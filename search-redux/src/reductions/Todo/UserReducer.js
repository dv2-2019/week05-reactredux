import { FETCH_USER_SUCCESS, FETCH_USER_BEGIN, FETCH_USER_ERROR } from "../../actions/UserAction"


const initialState = {
    user: {},
    loading: false,
    error: ''
}

export const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_USER_SUCCESS:
            return {
                user: action.payLoad,
                loading: false,
                error: ''
            }
        case FETCH_USER_BEGIN:
            return {
                ...state,
                loading: true
            }
        case FETCH_USER_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payLoad
            }
        default:
            return state
    }
}
