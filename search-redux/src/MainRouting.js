import React from 'react';
import { Route, BrowserRouter, Redirect } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import UserPhoto from './pages/UserPhoto';
import UserAlbums from './pages/UserAlbums';
import TodoRedux from './/components/Todo/TodoRedux';
import UsersRedux from './components/User/UserRedux';
import LoginPage from './pages/LoginPage';
import { isLogin, isLogout } from './actions/AuthAction';
import { connect } from 'react-redux';
import PrivateRoute from './PrivateRoute';

// const PrivateRoute = ({ path, component: Component }) => (
//     <Route path={path} render={(props) => {
//         const isLogin = localStorage.getItem(`isLogin`)

//         if (isLogin == 'true') {
//             return <Component {...props} />
//         } else {
//             return <Redirect to="/login" />
//         }
//     }} />
// )

const MainRouting = (props) => {
    const { isLogin, isLogout } = props;

    return (
        <BrowserRouter>
            <Route path="/processLogin" render={() => {
                isLogin()
                return <Redirect to="/users" />
            }} />
            <Route path="/processLogout" render={() => {
                isLogout()
                return <Redirect to="/login" />
            }} />
            <Route path="/" component={LoginPage} exact={true}></Route>
            <Route path="/login" component={LoginPage}></Route>
            <PrivateRoute path="/users" component={UsersRedux} exact={true}></PrivateRoute>
            <PrivateRoute path="/users/:user_id/todo" component={TodoRedux}></PrivateRoute>
            <PrivateRoute path="/users/:user_id/albums" component={UserAlbums} exact={true}></PrivateRoute>
            <PrivateRoute path="/users/:user_id/albums/:album_id/photos" component={UserPhoto}></PrivateRoute>
        </BrowserRouter>
    )
}

const mapStateToProps = state => {
    
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({ isLogin, isLogout }, dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps )(MainRouting);

