
import React from 'react';
import 'antd/dist/antd.css';
import { Form, Icon, Input, Button, Card, Avatar, Col, Row } from 'antd';

const LoginPage = () => {

    return (
        <Card title="Login" hoverable style={{ padding: '1em', marginTop: '8em', marginLeft: '35%', width: 450 }}>
            <Row>
                <Col span={12} offset={6}>
                    <Avatar src="https://www.mazadoka.com/wp-content/uploads/2019/10/mazadoka-profile.png" style={{ width: 200, height: 200 }} />
                </Col>

            </Row>

            <Form className="login-form">
                <Form.Item>
                    Username :
                    <Input
                        prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                        placeholder="Username"
                    />
                </Form.Item>
                <Form.Item>
                    Password :
                    <Input
                        prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                        type="password"
                        placeholder="Password"
                    />
                </Form.Item>
                <Form.Item>
                    <Button type="danger" htmlType="submit" className="login-form-button" block
                        onClick={
                            () => {
                                window.location = "/processLogin"
                            }
                        } >
                            Log in
                </Button>
                </Form.Item>
            </Form>
        </Card>

    )
}
export default LoginPage;