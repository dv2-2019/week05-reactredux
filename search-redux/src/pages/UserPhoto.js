// import React, { useState, useEffect } from 'react';
// import { List, Card, Row, PageHeader } from 'antd';
// const UserPhoto = (props) => {

//     const [photos, setPhotos] = useState([]);

//     useEffect(() => {
//         console.log('useEffect')
//         fetchAlbumPhotos();
//     }, [])


//     const fetchAlbumPhotos = () => {
//         const albumId = props.match.params.album_id;
//         console.log('fetchAnimeData')
//         fetch('http://jsonplaceholder.typicode.com/photos?albumId=' + albumId)
//             .then(response => response.json())
//             .then(data => {
//                 setPhotos(data)
//                 console.log(data)
//             })
//             .catch(error => console.log(error));
//     }

//     return (
//         <Row style={{ backgroundColor: '#e6e6e6' }}>
//             <PageHeader
//                 ghost={false}
//                 onBack={() => window.history.back()}
//                 title="Back"
//             ></PageHeader>
//             <Card style={{ margin: '4em' }}>
//                 <h2>Album No. {props.match.params.album_id}</h2>
//                 <List
//                     grid={{
//                         gutter: 16,
//                         xs: 1,
//                         sm: 2,
//                         md: 4,
//                         lg: 4,
//                         xl: 4,
//                         xxl: 3,
//                     }}
//                     dataSource={photos}
//                     renderItem={item => (
//                         <List.Item style={{ margin: '3em' }}>
//                             <Card title={item.title} > <img src={item.url} style={{ width: 250, marginLeft: '3em' }} /></Card>
//                         </List.Item>
//                     )}
//                 /></Card>
//         </Row>
//     )
// }
// export default UserPhoto;